<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategoriaSeeder extends Seeder
{

    public function run(): void
    {
        DB::table('categorias')->insert(
            [
                ['titulo' => 'Lanche', 'icone' => 'storage/icones/iconPizza.svg'],
                ['titulo' => 'Pizza', 'icone' => 'storage/icones/iconPizza.svg'],
                ['titulo' => 'Porção', 'icone' => 'storage/icones/iconPizza.svg'],
                ['titulo' => 'Bebida', 'icone' => 'storage/icones/iconPizza.svg'],
                ['titulo' => 'Refri', 'icone' => 'storage/icones/iconPizza.svg'],
            ]

        );
    }
}
