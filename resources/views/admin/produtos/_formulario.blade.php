@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="titulo" class="form-label">Título</label>
    <input type="text" class="form-control @error('titulo') is-invalid @enderror" name="titulo" id="titulo" placeholder="Insira o Título"
        value="{{ old('titulo', $produto->titulo) }}">
</div>

<div class="col-md-12">
    <label for="categoria" class="form-label">Categoria</label>
    <select class="form-control @error('categorias') is-invalid @enderror" name='categoria_id'>
        <option>Selecione a categoria</option>

        @foreach ($categorias as $categoria)
            <option value="{{ $categoria->id }}" @if ($produto->categoria_id == $categoria->id)
                selected
            @endif>{{ $categoria->titulo }}</option>
        @endforeach

    </select>
</div>

<div class="col-md-12">
    <label for="descricao" class="form-label">Descrição</label>
    <textarea class="form-control @error('descricao') is-invalid @enderror" name="descricao" id="descricao" rows="10"
        >{{ old('descricao', $produto->descricao) }}</textarea>
</div>

<div class="col-md-1">
    <label for="valor" class="form-label @error('valor') is-invalid @enderror">Valor</label>
    <input type="text" class="valor" name="valor" id="valor" value="{{ old('valor', $produto->valor) }}">
</div>

<div class="col-md-12">

    @if ($produto->foto)
        <img src="{{ $produto->foto }}" alt="" width="100">
    @endif

    <label for="foto" class="form-label">Foto</label>
    <input type="file" class="form-control @error('foto') is-invalid @enderror" name="foto" id="foto">
</div>


<div class="col-12">
    <button type="submit" class="btn btn-primary">Salvar</button>
</div>
