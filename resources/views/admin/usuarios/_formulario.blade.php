@csrf

@if ($errors->any())

    <ul class="alert alert-danger p-2 list-unstyled">
        @foreach ($errors->all() as $erro)
            <li>{{ $erro }}</li>
        @endforeach
    </ul>

@endif

<div class="col-md-12">
    <label for="nome" class="form-label">Nome</label>
    <input type="text" class="form-control" @error('nome') is-invalid @enderror name="nome" id="nome"
        placeholder="Insira o Nome" value="{{ old('nome', $usuario->nome) }}">

</div>

<div class="col-md-12">
    <label for="email" class="form-label">E-mail</label>
    <input type="text" name="email" class="form-control" @error('email') is-invalid @enderror id="email"
        placeholder="Insira a E-mail" value="{{ old('email', $usuario->email) }}">

</div>
<div class="col-md-12">
    <label for="senha" class="form-label" @error('senha') is-invalid @enderror>Senha</label>
    <input type="password" name="password" class="form-control" id="password" placeholder="" value="{{old('password', $usuario->password)}}">

</div>

<div class="col-md-3">
    <label for="role" class="form-label">Perfil</label>
    <select class="form-control" id="role" name="role">
        <option value="cliente">Cliente</option>
        <option value="administrador">Administrador</option>
    </select>
</div>


<div class="col-md-12">
    @if ($usuario->imagem)
        <img src="{{ $usuario->imagem }}" alt="" width="80">
    @endif

    <label for="avatar" class="form-label">Avatar</label>
    <input type="file" class="form-control" @error('imagem') is-invalid @enderror name="imagem" id="imagem">

</div>

<div class="col-12">
    <button type="submit" class="btn btn-primary">Salvar</button>
</div>
