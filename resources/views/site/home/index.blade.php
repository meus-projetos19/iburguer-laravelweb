@extends('layouts.site')

@section('conteudo')

<div class="lista-categorias">
    <ul>
        <li>
            <div class="item-categorias">
                <i class="icon-hamburger"></i>
                <p>Lanches</p>
            </div>
        </li>

        <li>
            <div class="item-categorias">
                <i class="icon-pizza"></i>
                <p>Pizza</p>
            </div>
        </li>

        <li>
            <div class="item-categorias">
                <i class="icon-batata"></i>
                <p>Porções</p>
            </div>
        </li>

        <li>
            <div class="item-categorias">
                <i class="icon-refrigerante"></i>
                <p>Refri</p>
            </div>
        </li>

        <li>
            <div class="item-categorias">
                <i class="icon-bebida"></i>
                <p>Bebida</p>
            </div>
        </li>
    </ul>
</div>
<div class="lista-produtos">
    {{-- <div class="texto-titulo">
        <h1>Faça seu pedido já!</h1>
        <p>Escolha os melhores lanches da região.</p>
    </div> --}}
    <div class="item-produtos">
        <img src="/img/lanche.jpeg" alt="lanche" class="img-fluid">
        <h2>Torre Mega</h2>
        <small>Pão, queijo, cebola, alface, tomate e picles. Três hambúrgueres, Tamanho 60cm ou 30cm. </small>

        <div class="produto-footer">
            <span class="preco">R$ 29,99</span>

            <a href="#" class="btn-adicionar">
                <i class="icon-adicionar"></i>
            </a>
        </div>

    </div>

    <div class="item-produtos">
        <img src="/img/lanche.jpeg" alt="lanche" class="img-fluid">

        <h2>Torre Mega</h2>

        <small>Pão, queijo, cebola, alface, tomate e picles. Três hambúrgueres, Tamanho 60cm ou 30cm.
        </small>

        <div class="produto-footer">
            <span class="preco">R$ 29,99</span>

            <a href="#" class="btn-adicionar">
                <i class="icon-adicionar"></i>
            </a>
        </div>
    </div>
    <div class="item-produtos">
        <img src="/img/lanche.jpeg" alt="lanche" class="img-fluid">
        <h2>Torre Mega</h2>
        <small>Pão, queijo, cebola, alface, tomate e picles. Três hambúrgueres, Tamanho 60cm ou 30cm. </small>
        <div class="produto-footer">
            <span class="preco">R$ 29,99</span>

            <a href="#" class="btn-adicionar">
                <i class="icon-adicionar"></i>
            </a>
        </div>
    </div>

    <div class="item-produtos">
        <img src="/img/lanche.jpeg" alt="lanche" class="img-fluid">

        <h2>Torre Mega</h2>

        <small>Pão, queijo, cebola, alface, tomate e picles. Três hambúrgueres, Tamanho 60cm ou 30cm.
        </small>

        <div class="produto-footer">
            <span class="preco">R$ 29,99</span>

            <a href="#" class="btn-adicionar">
                <i class="icon-adicionar"></i>
            </a>
        </div>
    </div>
</div>
@endsection
