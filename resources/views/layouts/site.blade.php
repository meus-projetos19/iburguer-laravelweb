<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" contents="Burguer, as melhores lanchonetes da região">
    <meta name="keywords" content="hamburguer, lanche, porção, pizza, delivery">
    <title>IBurguer</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,500;0,600;0,800;1,400&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    {{-- pega o arquivo na pasta públic --}}
    <link rel="stylesheet" href="/icones/icones.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous" />
</head>

<body>

    <header id="cabecalho">
        <div id="barra-topo">
            <div id="btnMenu">
                <i class="icon-menu"></i>
            </div>
            <div id="logotipo">
                <h1><img src="/img/logotipo.png" alt="Logo IBurguer" height="40px"></h1>
            </div>



            <nav id="menu-principal">
                <button id="btnClose">&#10006;</button>
                <img src="/img/logotipo.png">

                <ul>

                    <li><a href="#">Home</a></li>
                    <li><a href="#">Lanchonetes</a></li>
                    <li><a href="#">Localização</a></li>
                    <li><a href="#">Contatos</a></li>
                </ul>
            </nav>
            <div id="overlay"></div>

            <div id="usuario">
                <img src="/img/usuario.jpg" alt="perfil" height="40px" class="formato-bolinha">
            </div>
        </div>

        <div id="pesquisar">
            <form action="" method="get">
                <input type="search" name="produto" placeholder="Quer comer o que?">

                <button type="submit">
                    <i class="icon-lupa"></i>
                </button>
            </form>
        </div>
    </header>

    <main id="conteudo">

        @yield('conteudo')

    </main>

    <footer id="rodape">
        <nav id="menu-rodape">
            <ul>
                <li>
                    <a href="#">
                        <i class="icon-casa"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-coracao"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-pedido"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-carrinho"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </footer>
    <script src="/js/main.js"></script>
</body>

</html>
