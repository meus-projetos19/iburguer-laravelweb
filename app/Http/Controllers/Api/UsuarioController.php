<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function usuario(Request $request)
    {
        $usuario = $request->user();
        $dadosUsuario = [
            'nome' => $usuario->nome,
            'email' => $usuario->email,
            'imagem' => url($usuario->imagem),

        ];

        return response()->json($dadosUsuario);
    }

    public function registrar(Request $request)
    {

    }

}
