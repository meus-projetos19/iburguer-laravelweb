<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{

    public function index()
    {
        $usuarios = User::paginate(15);

        return view('admin.usuarios.index', [
            'usuarios' => $usuarios
        ]);
    }

    public function create()
    {
        return view('admin.usuarios.cadastrar', [
            'usuario' => new User()
        ]);
    }

    public function store(Request $request)
    {

        $request->validate([
            'nome' => 'required',
            'email' => 'required',
            'password' => 'required',

        ]);

        $usuario = new User();
        $usuario->nome = $request->nome;
        $usuario->email = $request->email;
        $usuario->password = $request->password;

        if ($request->hasFile('imagem')) {

            //Upload com nome randomico do arquivo
            $imagemPath = $request->file('imagem')->store('public/perfilusuario');
            $usuario->imagem = Storage::url($imagemPath);
        }

        $usuario->save();

        return redirect()->route('admin.usuarios.index')->with('sucesso', 'Usuário cadastrada com sucesso!');
    }

    public function edit($id)
    {
        $usuario = User::findOrFail($id);

        return view('admin.usuarios.editar', [
            'usuario' => $usuario
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nome' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $usuario = User::findOrFail($id);
        $usuario->nome = $request->nome;
        $usuario->email = $request->email;
        $usuario->password = $request->password;

        if ($request->hasFile('imagem')) {

            Storage::delete('public/perfilusuario/' . basename($usuario->imagem));

            $imagemPath = $request->file('imagem')->store('public/perfilusuario');
            $usuario->imagem = Storage::url($imagemPath);
        }

        $usuario->save();

        return redirect()->route('admin.usuarios.index')->with('sucesso', 'Usuario atualizado com Sucesso!');
    }

    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        if ($usuario->delete()) {

            Storage::delete('public/perfilusuario/' . basename($usuario->imagem));

            return redirect()->route('admin.usuarios.index')
                ->with('sucesso', 'Usuario Excluido com Sucesso!');
        } else {

            return redirect()->route('admin.usuarios.index')
                ->with('erro', 'Houve um erro ao Excluir o registro!');
        }
    }
}
