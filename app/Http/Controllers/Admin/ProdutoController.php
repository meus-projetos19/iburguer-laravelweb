<?php

namespace App\Http\Controllers\Admin;


use App\Models\Produto;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProdutoController extends Controller
{

    public function index()
    {
        $produtos = Produto::paginate(15);

        return view(
            'admin.produtos.index',
            [
                'produtos' => $produtos
            ]
        );
    }

    public function create()
    {
        $categorias = Categoria::all();

        return view('admin.produtos.cadastrar', [
            'produto' => new Produto(),
            'categorias' => $categorias
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([

            'foto' => 'required',
            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required',
            'categoria_id' => 'required'
        ]);

        $produto = new Produto();
        $produto->titulo = $request->titulo;
        $produto->descricao = $request->descricao;
        $produto->valor = $request->valor;
        $produto->categoria_id = $request->categoria_id;



        if ($request->hasFile('foto')) {

            $fotoPath = $request->file('foto')->store('public/produtos');
            $produto->foto = Storage::url($fotoPath);
        }


        $produto->save();

        return redirect()->route('admin.produtos.index')
            ->with('sucesso', 'Produto Cadastrado com Sucesso!');
    }

    public function edit($id)
    {
        $produto = Produto::findOrFail($id);
        $categoria = Categoria::all();

        return view('admin.produtos.editar', [
            'produto' => $produto,
            'categorias' => $categoria
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([

            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required',
            'categoria_id' => 'required'
        ]);

        $produto =  Produto::findOrFail($id);

        $produto->titulo = $request->titulo;
        $produto->descricao = $request->descricao;
        $produto->valor = $request->valor;
        $produto->categoria_id = $request->categoria_id;


        if ($request->hasFile('foto')) {

            Storage::delete('public/produtos/' . basename($produto->foto));

            $fotoPath = $request->file('foto')->store('public/produtos');
            $produto->foto = Storage::url($fotoPath);
        }

        $produto->save();

        return redirect()->route('admin.produtos.index')
            ->with('sucesso', 'Produto Atualizado com Sucesso!');
    }

    public function destroy($id)
    {
        $produto = Produto::findOrFail($id);

        if ($produto->delete()) {

            Storage::delete('public/produtos/' . basename($produto->foto));

            return redirect()->route('admin.produtos.index')
                ->with('sucesso', 'Produto Excluido com Sucesso!');
        } else {

            return redirect()->route('admin.produtos.index')
                ->with('erro', 'Houve um erro ao Excluir o registro!');
        }
    }
}
